<?php

namespace Users\Oop;

use Bitrix\Main\UserTable;

Class Organizator extends Users
{
    private $event = 'event';
    
    /**
     * @param string $event
     */
    public function setEvent(string $event): void
    {
        $this->event = $event;
    }
    
    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }
    
    /**
     * @param int $id
     * @throws \Exception
     */
    public function banUser(int $id): void
    {
        UserTable::update($id, ['ACTIVE' => 'N']);
        echo "Пользователь с ID = $id успешно забанен";
    }
    
    /**
     * @param int    $id
     * @param string $event
     * @throws \Exception
     */
    public function inviteUserToEvent(int $id, string $event): void
    {
        UserTable::update($id, ['UF_EVENT' => $event]);
        echo "Пользователь с ID = $id приглашен на мероприятие";
    }
}
