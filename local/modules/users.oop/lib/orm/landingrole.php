<?php

namespace Users\Oop\Orm;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\UserTable;

Loc::loadMessages(__FILE__);

/**
 * Class LandingPropsTable
 * @package Get\Property\Orm
 */
class LandingRoleTable extends UserTable
{
    public static function getMap()
    {
        $arFields = parent::getMap();
        
        $arFields['USER_ROL'] = new Entity\ReferenceField(
            'USER_ROL',
            FieldEnumTable::class,
            Join::on('this.UF_ROLE', 'ref.ID')
        );
        
        return $arFields;
    }
}
