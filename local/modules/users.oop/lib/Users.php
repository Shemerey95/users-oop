<?php

namespace Users\Oop;

use Bitrix\Main\Mail\Event;
use Bitrix\Main\UserTable;

Class Users
{
    public    $name     = 'Name';
    public    $email    = 'Email';
    public    $id       = 'Id';
    protected $password = 'password';
    
    /**
     * @param string $password
     */
    protected function setPassword(string $password): void
    {
        $this->password = $password;
    }
    
    /**
     * @param string $password
     * @return string
     */
    protected function getPassword(string $password): string
    {
        return $this->password = $password;
    }
    
    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function GetUsers(): void
    {
        $result = UserTable::getList([
            'select' => [
                'ID',
                'NAME',
                'UF_ROLE',
                'UF_EVENT'
            ],
        ]);
        
        while ($arUser = $result->fetch()) {
            $this->arResult['USER'][] = $arUser;
        }
    }
    
    /**
     * @return string
     */
    public function getEmail(): string
    {
        Event::send([
            "LID"      => "s1",
            "C_FIELDS" => [
                "EMAIL"    => $this->email,
                "PASSWORD" => $this->getPassword($this->password),
            ],
        ]);
        return $this->email;
    }
    
    /**
     * Users constructor.
     * @param string $name
     * @param string $email
     * @param int    $id
     * @param string $password
     */
    public function __construct(string $name, string $email, int $id, string $password)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->setPassword($password);
    }
}
