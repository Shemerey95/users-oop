<?php

namespace Users\Oop;

use Bitrix\Main\UserTable;

Class Admin extends Users
{
    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @throws \Exception
     */
    public function addUser(string $name, string $email, string $password): void
    {
        UserTable::add([
            'NAME'     => $name,
            'EMAIL'    => $email,
            'PASSWORD' => $password,
        ]);
        
        echo "Пользователь успешно добавлен на сайт";
    }
    
    /**
     * @param string $role
     * @return bool
     */
    private function checkRoleAdmin(string $role): ?bool
    {
        if ($role != 'Администратор') {
            return true;
        }
    }
    
    /**
     * @param int    $id
     * @param string $role
     * @throws \Exception
     */
    public function deleteUser(int $id, string $role): void
    {
        if ($this->checkRoleAdmin($role)) {
            UserTable::delete($id);
            echo "Пользователь с Id = $id успешно удален";
        } else {
            echo "Пользователь является администратором";
        }
    }
    
    /**
     * @param int $id
     * @throws \Exception
     */
    public function banUser(int $id): void
    {
        UserTable::update($id, ['ACTIVE' => 'N']);
        echo "Пользователь с ID = $id успешно забанен";
    }
    
    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @throws \Exception
     */
    public function addOrganizator(string $name, string $email, string $password): void
    {
        UserTable::add([
            'NAME'     => $name,
            'EMAIL'    => $email,
            'PASSWORD' => $password,
            'UF_ROLE'  => 'Организатор',
        ]);
        
        echo "Организатор успешно добавлен";
    }
    
    /**
     * @param string $role
     * @return bool
     */
    private function checkRoleOrg(string $role): ?bool
    {
        if ($role !== 'Администратор') {
            return true;
        }
    }
    
    /**
     * @param int    $id
     * @param string $role
     * @throws \Exception
     */
    public function deleteOrganizator(int $id, string $role): void
    {
        if ($this->checkRoleOrg($role)) {
            UserTable::delete($id);
            echo "Организатор успешно удалён";
        } else {
            echo "Пользователь не является организатором";
        }
    }
}
