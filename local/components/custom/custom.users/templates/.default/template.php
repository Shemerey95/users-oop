<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
$this->setFrameMode(true);
?>

<div class="content">
    <h2><?= $title = ($arResult) ? $arResult["USER"]["USER_ROLE"] : 'Пожалуйста зарегистрируйтесь или войдите в систему!'?></h2>
    
    <?php if ($arResult) {
        echo '<pre>';
        print_r($arResult);
        echo '</pre>';
    }?>
    
    <?php if ($arResult["USER"]["USER_ROLE"] === 'Администратор'): ?>
        <form method="post" action="#">
            <button>Добавить пользователя</button>
        </form>
        <form method="post" action="#">
            <button>Удалить пользователя</button>
        </form>
        <form method="post" action="#">
            <button>Забанить пользователя как спамера</button>
        </form>
        <form method="post" action="#">
            <button>Добавить организатора</button>
        </form>
        <form method="post" action="#">
            <button>Удалить организатора</button>
        </form>
        <form method="post" action="#">
            <button>Получить письмо восстановления пароля</button>
        </form>
    <?php elseif ($arResult["USER"]["USER_ROLE"] === 'Организатор'): ?>
        <form method="post" action="#">
            <button>Пригласить пользователя на свое мероприятие</button>
        </form>
        <form method="post" action="#">
            <button>Забанить пользователя как спамера</button>
        </form>
        <form method="post" action="#">
            <button>Получить письмо восстановления пароля</button>
        </form>
    <?php elseif ($arResult["USER"]["USER_ROLE"] === 'Пользователь'): ?>
        <form method="post" action="#">
            <button>Получить письмо восстановления пароля</button>
        </form>
    <?php else:?>
    <?php endif; ?>
</div>
