<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Users\Oop\Orm\LandingRoleTable,
    \Users\Oop\Users,
    \Users\Oop\Admin,
    \Users\Oop\Organizator;

/**
 * Class ExampleCompSimple
 */
class ExampleCompSimple extends CBitrixComponent
{
    public  $arItem = [];
    private $_request;
    
    /**
     * Проверка наличия модулей, требуемых для работы компонента
     * @return bool
     * @throws Exception
     */
    private function _checkModules()
    {
        if (!Loader::includeModule('users.oop')) {
            throw new \Exception('Не загружены модули необходимые для работы модуля');
        }
        return true;
    }
    
    /**
     * Подготовка параметров компонента
     *
     * @param $arParams
     *
     * @return mixed
     */
    public function onPrepareComponentParams($arParams)
    {
        // тут пишем логику обработки параметров, дополнение параметрами по умолчанию
        // и прочие нужные вещи
        
        return $arParams;
    }
    
    /**
     * Получение элементов инфоблока
     *
     * @param $arParams
     *
     * @return string
     */
    public function getСurrentUser()
    {
        
        $query = LandingRoleTable::query()
            ->setSelect(
                [
                    'ID',
                    'NAME',
                    'PASSWORD',
                    'USER_ROLE' => 'USER_ROL.VALUE',
                ]
            )
            ->where('ID', CUser::GetID());
        
        $result = $query->exec();
        
        while ($arUser = $result->fetch()) {
            $arResult["USER"] = $arUser;
        }
        
        $this->arResult = $arResult;
    }
    
    /**
     * @param array $arResult
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function selectUserRights(array $arResult): void
    {
        $request      = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        $comand       = $request->getPost('comand');
        $userid       = $request->getPost('id');
        $useremail    = $request->getPost('email');
        $userpassword = $request->getPost('password');
        $userrole     = $request->getPost('role');
        
        
        if ($arResult["USER"]["USER_ROLE"] === 'Администратор') {
            $admin = new Admin($arResult["USER"]["NAME"], $arResult["USER"]["EMAIL"], $arResult["USER"]["ID"], $arResult["USER"]["PASSWORD"]);
            
            $admin->GetUsers();
            
            switch ($comand) {
                case 'addUser':
                    $admin->addUser($userid, $useremail, $userpassword);
                    break;
                case 'deleteUser':
                    $admin->deleteUser($userid, $userrole);
                    break;
                case 'banUser':
                    $admin->banUser($userid);
                    break;
                case 'addOrganizator':
                    $admin->addOrganizator($userid, $useremail, $userpassword);
                    break;
                case 'deleteOrganizator':
                    $admin->deleteOrganizator($userid, $userrole);
                    break;
                case 'getEmail':
                    $admin->getEmail();
            }
        } elseif ($arResult["USER"]["USER_ROLE"] === 'Организатор') {
            $organizator = new Organizator($arResult["USER"]["NAME"], $arResult["USER"]["EMAIL"], $arResult["USER"]["ID"], $arResult["USER"]["PASSWORD"]);
            
            $organizator->GetUsers();
            
            switch ($comand) {
                case 'banUser':
                    $organizator->organizator($userid);
                    break;
                case 'inviteUserToEvent':
                    $organizator->inviteUserToEvent($userid, $userrole);
                    break;
                case 'getEmail':
                    $organizator->getEmail();
            }
        } else {
            $user = new Users($arResult["USER"]["NAME"], $arResult["USER"]["EMAIL"], $arResult["USER"]["ID"], $arResult["USER"]["PASSWORD"]);
            $user->GetUsers();
            $user->getEmail();
        }
    }
    
    /**
     * Запуск компонента
     * @return mixed|void
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent()
    {
        $this->_checkModules();
        
        $this->_request = Application::getInstance()->getContext()->getRequest();
        
        $this->getСurrentUser();
        
        // $this->selectUserRights(array $this->arResult);
        
        $this->includeComponentTemplate();
    }
}
