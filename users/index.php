<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пользователи");
?>

<?php $APPLICATION->IncludeComponent(
    "custom:custom.users",
    ".default",
    array(
        "IBLOCK_ID" => "4",
        "COMPONENT_TEMPLATE" => ".default",
        "IBLOCK_TYPE" => "Linux_Soft",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
    ),
    false
);?>

<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
